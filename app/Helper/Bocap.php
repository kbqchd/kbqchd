<?php

namespace App\Helper;

class Bocap{
    public static function getSignFromSlug($slug){
        switch ($slug) {
            case 'bach-duong': return 1;
            case 'kim-nguu': return 2;
            case 'song-tu': return 3;
            case 'cu-giai': return 4;
            case 'su-tu': return 5;
            case 'xu-nu': return 6;
            case 'thien-binh': return 7;
            case 'than-nong': return 8;
            case 'nhan-ma': return 9;
            case 'ma-ket': return 10;
            case 'bao-binh': return 11;
            case 'song-ngu': return 12;
        }
        abort(404);
    }
    public static function getSlug($id){
        switch ($id) {
            case 1: return 'bach-duong';
            case 2: return 'kim-nguu';
            case 3: return 'song-tu';
            case 4: return 'cu-giai';
            case 5: return 'su-tu';
            case 6: return 'xu-nu';
            case 7: return 'thien-binh';
            case 8: return 'than-nong';
            case 9: return 'nhan-ma';
            case 10: return 'ma-ket';
            case 11: return 'bao-binh';
            case 12: return 'song-ngu';
        }
        abort(404);
    }
}