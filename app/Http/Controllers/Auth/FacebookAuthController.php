<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use App\User;

class FacebookAuthController extends Controller
{
	public function redirect()
    {
        return Socialite::driver('facebook')->fields([
            'first_name', 'last_name', 'email', 'gender', 'birthday','link'
        ])->scopes([
            'email', 'public_profile', 'user_birthday'
        ])->redirect();
    }

    public function callback()
    {
        $redirectUrl = '/home';
        $fUser = Socialite::driver('facebook')->fields([
            'first_name', 'last_name', 'name', 'email', 'gender', 'birthday','link'
        ])->user();
        $user = User::where('facebook_id',$fUser->id)->first();
        if(!$user){
            $user = new User();
            $user->password = str_random(30);
            $user->facebook_id = $fUser->id;
            if (isset($fUser->user['birthday'])) {
                $redirectUrl = '/user/birthday';
            }
        }
        $user->email = $fUser->email;
        $user->facebook_token = $fUser->token;
        $user->name = $fUser->name;
        $user->gender = $fUser->user['gender'];
        $user->first_name = $fUser->user['first_name'];
        $user->last_name = $fUser->user['last_name'];
        $user->facebook_url = $fUser->profileUrl;
        $user->avatar = \App\Helper\ConvertHelper::removeTypeLink($fUser->avatar);

        if(isset($fUser->user['birthday'])){
            $user->birthday = $fUser->user['birthday'];
        }

        $user->save();
        \Auth::login($user);
        return redirect($redirectUrl);
    }
}
