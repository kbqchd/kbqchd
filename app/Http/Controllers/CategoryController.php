<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Helper\Bocap;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getViewBySlug($slug){
        $id = Bocap::getSignFromSlug($slug);
        if (\Auth::check()) {
            $users = User::where('id','!=',\Auth::user()->id)
                        ->orderBy('created_at','desc')
                        ->get();
        }else{
            $users = User::orderBy('created_at','desc')
                        ->get();
        }
        $selectHoroscope = '';
        $horoscopeId = $id;
        $result = [];
        $horoscope = \App\User::$horoscope;
        foreach ($horoscope as $key => $horo) {
            if ($key == $id) {
                $selectHoroscope = $horo;
            }
        }
        foreach ($users as $user) {
            if ($user->birthday AND $user->getHoroscope() == $selectHoroscope['name']) {
                $result[] = $user;
            }
        }
        return view('category.horoscope',compact('result','slug','horoscope','selectHoroscope','horoscopeId'));
    }
    public function getView($id){
        $users = User::where([['id','!=',\Auth::user()->id],['birthday','!=',null]])->orderBy('created_at','desc')->get();
        $horoscopeName = '';
        $horoscopeId = $id;
        $result = [];
        $horoscope = \App\User::$horoscope;
        foreach ($horoscope as $key => $horo) {
            if ($key == $id) {
                $horoscopeName = $horo['name'];
            }
        }
        foreach ($users as $user) {
            if ($user->birthday AND $user->getHoroscope() == $horoscopeName) {
                $result[] = $user;
            }
        }
        return view('category.horoscope',compact('result', 'horoscope','horoscopeName','horoscopeId'));
    }
}
