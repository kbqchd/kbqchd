<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereNotNull('birthday')->orderBy('created_at','desc')->get();
        $otherUsers = User::whereNull('birthday')->orderBy('created_at','desc')->get();
        $horoscope = \App\User::$horoscope;
        $result = [1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => [], 7 => [], 8 => [], 9 => [], 10 => [], 11 => [], 12 => []];
        foreach ($horoscope as $key => $horo) {
            foreach ($users as $user) {
                if ($user->birthday && $user->getHoroscope() == $horo['name']) {
                    $result[$key][] = $user;
                }
            }
        }
        $takeTenNewUsers = User::orderBy('created_at','desc')->take(17)->get();
        return view('welcome',compact('users','horoscope','result','otherUsers','takeTenNewUsers'));
    }
}
