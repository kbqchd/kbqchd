<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Image;

class SloganController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    public function getMinhThichThiMinhLamThoi(){
        $user = \Auth::user();
        return view('test.minhThichThiMinhLamThoi',compact('user'));
    }
    public function postMinhThichThiMinhLamThoi(Request $request){
        $user = \Auth::user();
        $keyword = \App\Helper\ConvertHelper::removeBackspace($request->keyword);
        $validator = \Validator::make(\Input::all(),['keyword' => 'max:7']);
        if ($validator->fails()) {
            \Session::flash('error','Bạn chỉ được nhập một từ dưới 8 ký tự!');
            return back()->widthInput();
        }
        $text = 'Mình thích thì mình "'.$keyword.'" thôi';
        $user = \Auth::user();
        $img = Image::make($user->avatar.'?width=500&height=500');
        $imgBg = Image::make('images/bg-white.jpg');
        $imgBg->resize(700, 105);
        $imgBg->opacity(70);
        $img->insert($imgBg, 'bottom');
        $img->text($text, 335, 640, function($font) {
            $font->size('35');
            $font->file('fonts/ARIALN.TTF');
            $font->color('#000000');
            $font->align('center');
            $font->valign('bottom');
        });
        $fileName = 'uploads/'.time().'_'.$user->id.'.jpg';
        $img->save($fileName);
        //return $img->response('png');
        return view('test.minhThichThiMinhLamThoi',compact('user','fileName'));
    }
    public function postDeleteImage(Request $request){
        \File::delete($request->fileName);
        return redirect('/slogan/minh-thich-thi-minh-lam-thoi');
    }
}
