<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getIndex(){
        $users = User::all();
        return view('user.index',compact('users'));
    }
    public function getBirthday(){
        $user = User::find(\Auth::user()->id);
        return view('user.birthday',compact('user'));
    }
    public function postBirthday(Request $request){
        $validator = \Validator::make(\Input::all(), ['birthday'  => 'required|date_format:m/d/Y']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $user = User::find(\Auth::user()->id);
        $user->birthday = $request->birthday;
        $user->save();
        return redirect('/user/account');
    }
    public function getCreate(){
        return view('user.create');
    }
    public function postCreate(Request $request){
        $rules = ['facebook_id' => 'required|unique:users|max:255',
                  'first_name' => 'required|max:255',
                  'birthday' => 'date_format:m/d/Y'];
        $validator = \Validator::make(\Input::all(),$rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $facebook_id = \App\Helper\ConvertHelper::removeBackspace($request->facebook_id);
        $user = new User();
        $user->facebook_id = $facebook_id;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->name = $request->first_name.' '.$request->last_name;
        if ($request->birthday) {
            $user->birthday = $request->birthday;
        }
        $user->gender = $request->gender;
        $user->facebook_url = 'https://www.facebook.com/app_scoped_user_id/'.$facebook_id.'/';
        $user->avatar = 'https://graph.facebook.com/v2.8/'.$facebook_id.'/picture';
        $user->password = str_random(30);
        $user->save();
        \Session::flash('success','Success.');
        return back();
    }
    public function getAccount(){
        $user = User::find(\Auth::user()->id);
        return view('user.account',compact('user'));
    }
    public function getAll(){
        $users = User::orderBy('created_at','desc')->get();
        return view('user.all',compact('users'));
    }
}
