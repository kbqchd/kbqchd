<?php

namespace App;

use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Notifiable, Authenticatable, Authorizable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','facebook_id','first_name','last_name','birthday','facebook_url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static $horoscope =  [1 => ['name' => 'Bạch Dương', 'color' => 'red', 'date' => '21/3-19/4'],
                                2 => ['name' => 'Kim Ngưu', 'color' => 'green', 'date' => '20/4-20/5'],
                                3 => ['name' => 'Song Tử', 'color' => 'orange', 'date' => '21/5-21/6'],
                                4 => ['name' => 'Cự Giải', 'color' => 'violet', 'date' => '22/6-22/7'],
                                5 => ['name' => 'Sư Tử', 'color' => 'yellow', 'date' => '23/7-22/8'],
                                6 => ['name' => 'Xử Nữ', 'color' => 'blue', 'date' => '23/8-22/9'],
                                7 => ['name' => 'Thiên Bình', 'color' => 'green', 'date' => '23/9-23/10'],
                                8 => ['name' => 'Thần Nông', 'color' => 'red', 'date' => '24/10-21/11'],
                                9 => ['name' => 'Nhân Mã', 'color' => 'violet', 'date' => '22/11-21/12'],
                                10 => ['name' => 'Ma Kết', 'color' => 'indigo', 'date' => '22/12-19/1'],
                                11 => ['name' => 'Bảo Bình', 'color' => 'violet', 'date' => '20/1-18/2'],
                                12 => ['name' => 'Song Ngư', 'color' => 'indigo', 'date' => '19/2-20/3']];
    public function getHoroscope(){
        $result = '';
        $year = date('Y',strtotime($this->birthday));
        $birthday = $this->birthday;
        if ($birthday == null) {
            return 'không xác định';
        }
        if (strtotime($year.'-03-21') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-04-19')) {
            $result = 'Bạch Dương';
        }elseif (strtotime($year.'-04-20') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-05-20')) {
            $result = 'Kim Ngưu';
        }elseif (strtotime($year.'-05-21') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-06-21')) {
            $result = 'Song Tử';
        }elseif (strtotime($year.'-06-22') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-07-22')) {
            $result = 'Cự Giải';
        }elseif (strtotime($year.'-07-23') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-08-22')) {
            $result = 'Sư Tử';
        }elseif (strtotime($year.'-08-23') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-09-22')) {
            $result = 'Xử Nữ';
        }elseif (strtotime($year.'-09-23') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-10-23')) {
            $result = 'Thiên Bình';
        }elseif (strtotime($year.'-10-24') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-11-21')) {
            $result = 'Thần Nông';
        }elseif (strtotime($year.'-11-22') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-12-21')) {
            $result = 'Nhân Mã';
        }elseif (strtotime($year.'-01-20') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-02-18')) {
            $result = 'Bảo Bình';
        }elseif (strtotime($year.'-02-19') <= strtotime($birthday) && strtotime($birthday) <= strtotime($year.'-03-20')) {
            $result = 'Song Ngư';
        }else {
            $result = 'Ma Kết';
        }
        return $result;
        // Bạch Dương (21/3-19/4)  Kim Ngưu (20/4-20/5)    Song Tử (21/5-21/6) Cự Giải (22/6-22/7)
        // Sư Tử (23/7-22/8)   Xử Nữ (23/8-22/9)   Thiên Bình (23/9-23/10) Thần Nông (24/10-21/11)
        // Nhân Mã (22/11-21/12)   Ma Kết (22/12-19/1) Bảo Bình (20/1-18/2)    Song Ngư (19/2-20/3)
    }
    public function getAge(){
        $thisYear = date('Y');
        $yearofBirth = date('Y',strtotime($this->birthday));
        return $thisYear - $yearofBirth;
    }

    public function getShortName(){
        if($this->first_name && $this->last_name){
            return $this->first_name.' '.$this->last_name;
        }
        return $this->name;
    }
}
