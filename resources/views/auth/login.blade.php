@extends('layouts.app')

@section('content')
<style type="text/css">
    .box-login{
        background-image: url('/images/login-bg.jpg');
        background-size: cover;
        color: #fff;
        padding: 10px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <h3 style="color:red;"><span class="glyphicon glyphicon-lock"></span>Chào mừng bạn đến với website Bọ Cạp</h3>
                    <div class="row">
                        <div class="box-login">
                            <label>Vui lòng đăng nhập để tiếp tục:</label>
                            <h3>- Biết được bạn thuộc cung nào</h3>
                            <h3>- Tìm kiếm và kết bạn với các cung hoàng đạo khác</h3>
                            <h3>- Sử dụng vô số các tính năng thú vị dành riêng cho bạn</h3>
                        </div>
                    </div>
                    <br>
                    <a class="btn btn-primary btn-lg pull-left" href="/auth/facebook"><i class="fa fa-lg fa-facebook-square"></i> Đăng nhập qua Facebook</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
