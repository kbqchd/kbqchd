@extends('layouts.app')

@section('content')
<div class="col-md-12">

    <div class="clearfix">
        <ul class="list-inline list-title-category">
            @foreach($horoscope as $key => $horo)
                @if($key == $horoscopeId)
                <li><a href="/{{App\Helper\Bocap::getSlug($key)}}" style="color:#fff;background-color: {{$horo['color']}}"><label>{{$horo['name']}}</label></a></li>
                @else
                <li><a href="/{{App\Helper\Bocap::getSlug($key)}}"><label>{{$horo['name']}}</label></a></li>
                @endif
            @endforeach
        </ul>
        <img src="/images/{{$horoscopeId}}.png" alt="{{$selectHoroscope['name']}}" class="img-responsive element-center img-horoscope">
        <p class="text-center text-muted m-t-5">{{$selectHoroscope['date']}}</p>
        <p class="text-muted text-center m-t-5">Xem những ai thuộc cung <label>{{$selectHoroscope['name']}} </label> nhé!</p>
    </div>
    <br>
    @foreach($result as $user)
        <a href="{{$user->facebook_url}}" target="_blank" class="item-index"><img data-original="{{$user->avatar}}?height=75&width=75" class="img-responsive img-hover-shadow lazy" height="75" width="75">{{$user->name}}</a>
    @endforeach
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="fb-like" data-href="http://bocap.net" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        </div>
        <div class="col-md-12">
            <div class="fb-comments" data-href="http://bocap.net/{{$slug}}" data-numposts="5"></div>
        </div>
    </div>
</div>
@endsection
