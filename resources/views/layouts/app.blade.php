<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('extraheads')

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="/images/icon_favicon.png" type="image/png" />
    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style-index.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="/js/jquery.lazyload.min.js"></script>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-theme navbar-fixed-top">
            <div class="">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="/images/logo.png" class="img-responsive hidden-xs" style="max-height: 51px" alt="Kết bạn qua cung hoàng đạo">
                        <img src="/images/logo.png" class="img-responsive visible-xs" style="max-height: 30px" alt="Kết bạn qua cung hoàng đạo">
                    </a>

                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav visible-xs">
                        @foreach(\App\User::$horoscope as $id => $horo)
                        <li><a href="/category/{{$id}}"><i class="fa fa-star" style="color: {{$horo['color']}}"></i> {{$horo['name']}}</a></li>
                        @endforeach
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <li><a href="http://quiz.bocap.net">Bọ cạp Quiz</a></li>
                        <!-- <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Slogan</a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/slogan/minh-thich-thi-minh-lam-thoi">Mình thích thì mình...thôi</a></li>
                            </ul>
                        </li> -->
                        <li class="dropdown hidden-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-list"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach(\App\User::$horoscope as $id => $horo)
                                <li><a href="/{{App\Helper\Bocap::getSlug($id)}}"><i class="fa fa-star" style="color: {{$horo['color']}}"></i> {{$horo['name']}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @if (Auth::guest())
                            <a href="{{ url('/auth/facebook') }}" class="btn btn-primary" style="margin: 10px 20px; color: #fff">Login Facebook</a>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding: 7px 30px 7px 15px">
                                    <img src="{{ Auth::user()->avatar }}" class="img-circle" height="35" width="35">
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/user/account">{{Auth::user()->name}}</a></li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Đăng xuất <i class="fa fa-sign-out"></i>
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container" style="margin-top: 65px">
            @yield('content')
        </div>

    </div>
    <div class="col-md-12 footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="fb-page" data-href="https://www.facebook.com/ketbanquacunghoangdao/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ketbanquacunghoangdao/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ketbanquacunghoangdao/">Kết bạn qua cung hoàng đạo</a></blockquote></div>
                </div>
                <div class="col-md-9">
                    <ul class="list-inline">
                        @foreach(\App\User::$horoscope as $key => $horoscope)
                        <li><a href="/{{App\Helper\Bocap::getSlug($key)}}">{{$horoscope['name']}}</a></li>
                        @endforeach
                    </ul>
                    <p class="text-center">Copyright © 2017 | Phone: 0167 7713 817 | Email: thanhfreewings@gmail.com</p>
                </div>
            </div>
        </div>
    </div>
    @yield('extrascripts')
    <script type="text/javascript">
        $(function() {
            $("img.lazy").lazyload({
                effect : "fadeIn"
            });
        })
    </script>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1776170392647916',
          xfbml      : true,
          version    : 'v2.8'
        });
        FB.AppEvents.logPageView();
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
</body>
</html>
