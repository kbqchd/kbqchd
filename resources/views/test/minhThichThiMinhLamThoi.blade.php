@extends('layouts.app')

@section('extraheads')
    <meta property="og:url" content="http://bocap.net/slogan/minh-thich-thi-minh-lam-thoi">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Sáng tạo ra slogan của bạn">
    <meta property="og:description" content="slogan">
    <meta property="og:image" content="http://bocap.net/images/logo.png">
@endsection

@section('content')
<div class="col-md-12">
    <br>
    <div class="col-md-6 panel p-t-10 p-b-10">
        @if(isset($fileName))
        <div class="image-text">
            <img src="/{{$fileName}}" alt="Mình thích thì mình làm thôi" class="img-responsive element-center">
            <button class="btn btn-primary btn-block m-t-10" id="share">Chia sẻ ngay qua Facebook</button>
        </div>
        <form action="/slogan/delete" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="fileName" value="{{$fileName}}">
            <button class="btn m-t-10 m-b-10" id="again">Thử lại</button>
        </form>
        @else
        <h1>Mình thích thì mình "..." thôi!</h1>
        <br>
        <p class="text-muted">Hãy điền <span class="text-danger">1 từ</span> vào dấu '...' theo ý của bạn</p>
        <form method="POST" id="myForm">
            {{ csrf_field() }}
            @if(Session::has('error'))
                <br><label class="text-danger">{{Session::get('error')}}</label><br>
            @endif
            <input type="text" name="keyword" data-rule-maxlength="8" data-msg-maxlength="Exactly 8 characters please" class="form-control" placeholder="Nhập từ khóa" required>
            <button class="btn btn-primary m-t-10 m-b-10" type="button" onclick="validate()">Xem kết quả</button>
        </form>
        @endif
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="fb-comments" data-href="http://bocap.net/slogan/minh-thich-thi-minh-lam-thoi" data-numposts="5"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function validate() {
        var keyword = $("input[name='keyword']").val().length;
        if (keyword == 0) {
            alert('Nhập một từ dưới 8 ký tự!');
        }else if(keyword < 8){
            $('#myForm').submit();
        }else{
            alert('Bạn chỉ được nhập một từ dưới 8 ký tự!');
        }
    }
    $(function(){
        $('#share').click(function(){
            FB.ui({
                method: 'share',
                href: 'http://bocap.net/slogan/minh-thich-thi-minh-lam-thoi',
              },
              // callback
              function(response) {
                if (response && !response.error_message) {
                    $('#share').addClass('hidden');
                    $('#again').addClass('hidden');
                    alert('Posting completed.');
                } else {
                  alert('Error while posting.');
                }
            });
        })
    })
</script>
@endsection
