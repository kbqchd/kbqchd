@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(!$user->birthday)
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Hãy cho mọi người biết bạn thuộc cung nào bằng cách <a href="/user/birthday">cập nhật</a> ngày sinh của bạn.
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Tài khoản</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{$user->avatar}}?height=100&width=100" alt="Ảnh đại diện của bạn"><br>
                            <span class="label label-info" style="padding: 2px 20px">ảnh đại diện</span>
                        </div>
                        <div class="col-md-9">
                            <p class="text-muted">Facebook: <a href="{{$user->facebook_url}}" target="_blank"><label>{{$user->name}}</label></a></p>
                            <p class="text-muted">Email: <span class="text-info">{{$user->email}}</span></p>
                            @if($user->birthday)
                            <p class="text-muted">Ngày sinh: {{substr($user->birthday,3,2)}} tháng {{substr($user->birthday,0,2)}}, {{substr($user->birthday,6)}} (private) <a href="/user/birthday" class="pull-right"><small><i class="fa fa-pencil"></i> thay đổi</small></a></p>
                            @else
                            <p class="text-muted">Ngày sinh:<a href="/user/birthday" class="pull-right"><small><i class="fa fa-pencil"></i> nhập ngày sinh</small></a></p>
                            @endif
                            <p class="text-muted">Cung hoàng đạo: {{$user->getHoroscope()}}</p>
                            <p class="text-muted">Quốc gia: {{$user->country}}</p>
                            <p class="text-muted">Giới tính: @if($user->gender == 'male') Nam @else Nữ @endif</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
