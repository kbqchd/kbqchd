@extends('layouts.app')

@section('content')
<div class="col-md-12">
    <br>
    <p class="text-muted">Tất cả người dùng</p>
    @foreach($users as $user)
        <a href="{{$user->facebook_url}}" target="_blank" data-toggle="tooltip" data-placement="top" title="{{$user->getHoroscope()}}" class="item-index"><img data-original="{{$user->avatar}}?height=75&width=75" class="img-responsive img-hover-shadow lazy" height="75" width="75">{{$user->name}}</a>
    @endforeach
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="fb-like" data-href="http://bocap.net" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        </div>
        <div class="col-md-12">
            <div class="fb-comments" data-href="http://bocap.net/user/all" data-numposts="5"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@endsection
