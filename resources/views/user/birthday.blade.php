@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ngày sinh của bạn</div>
                <div class="panel-body">
                    <div class="col-md-6 col-md-offset-3">
                        <form action="/user/birthday" method="POST">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input id="datepicker" type="text" class="form-control" name="birthday" placeholder="Nhập ngày sinh của bạn" value="@if(\Auth::user()->birthday){{\Auth::user()->birthday}}@endif" required>
                                <span class="input-group-btn">
                                    <button class="btn btn-success">Ok!</button>
                                </span>
                            </div>
                            <div class="form-group">
                                <p class="text-muted">tháng/ngày/năm (private)</p>
                                @if ($errors->has('birthday'))
                                    <br><label class="text-danger">{{ $errors->first('birthday') }}</label><br>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1950:2016',
            dateFormat: "mm/dd/yy"
        });
    } );
</script>
@endsection
