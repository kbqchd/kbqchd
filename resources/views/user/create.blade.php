@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create user</div>

                <div class="panel-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                    @endif
                    <form action="/user/create" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>facebook_id</label>
                            <input type="text" name="facebook_id" class="form-control" placeholder="facebook_id" required>
                            @if ($errors->has('facebook_id'))
                                <label class="text-danger">{{ $errors->first('facebook_id') }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>First name</label>
                            <input type="text" name="first_name" class="form-control" placeholder="first_name" required>
                            @if ($errors->has('first_name'))
                                <label class="text-danger">{{ $errors->first('first_name') }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Last name</label>
                            <input type="text" name="last_name" class="form-control" placeholder="last_name">
                            @if ($errors->has('last_name'))
                                <label class="text-danger">{{ $errors->first('last_name') }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>birthday m/d/Y</label>
                            <input type="text" name="birthday" class="form-control" placeholder="m/d/Y">
                            @if ($errors->has('birthday'))
                                <label class="text-danger">{{ $errors->first('birthday') }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="radio" name="gender" value="male" checked> male
                            <input type="radio" name="gender" value="female"> female
                            @if ($errors->has('gender'))
                                <label class="text-danger">{{ $errors->first('gender') }}</label>
                            @endif
                        </div>
                        <button class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
