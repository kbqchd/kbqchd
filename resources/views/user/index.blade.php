@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User</div>

                <div class="panel-body">
                    <table class="table">
                        @foreach($users as $user)
                        <tr>
                            <td><img src="{{$user->avatar}}" height="25" width="25"> {{$user->name}}</td>
                            <td>@if($user->birthday) {{date('d-m-Y',strtotime($user->birthday))}} @endif</td>
                            <td>@if($user->birthday) {{$user->getHoroscope()}} @endif</td>
                            <td><a href="{{$user->facebook_url}}" target="_blank">link</a></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
