@extends('layouts.app')

@section('content')
<style type="text/css">
    a.horo-title{
        font-size: 18px;
        cursor: pointer;
        font-weight: bold;
    }
    .category-box{
        position: relative;
        max-height: 500px;
        overflow: hidden;
        border: 1px solid #efefef;
        margin: 24px 0;
        margin-top: 0;
        background-color: #fff;
        padding: 12px;
        -webkit-box-shadow: 1px 1px 2px 1px rgba(199,191,199,1);
-moz-box-shadow: 1px 1px 2px 1px rgba(199,191,199,1);
box-shadow: 1px 1px 2px 1px rgba(199,191,199,1);
    }
    .category-box .view-more{
        position: absolute;bottom: 0;
        display: block;
        text-align: center;
        width: 100%;
        height: 120px;
        padding-top: 40px;
        line-height: 80px;
        font-size: 20px;
        left: 0;
        color: #efefef;
        font-weight: bolder;
        background-image: linear-gradient(to bottom, transparent, #00695C);
    }
    .new-user{
        border: 2px dashed #fff;
        padding: 5px;
        margin-bottom: 15px;
    }
    .new-user>a{
        margin: 0 5px 5px 0;
        float: left;
    }
</style>
<div class="col-md-12">
    @if(Auth::user() AND Auth::user()->birthday)
    <p class="text-muted text-center">Chào <b>{{\Auth::user()->getHoroscope()}}</b>, cùng tìm hiểu xem ai hợp với bạn nhé!</p>
    @endif
    @if(Auth::user() AND Auth::user()->birthday == null)
    <div class="row">
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Hãy cho mọi người biết bạn thuộc cung nào bằng cách cập nhật ngày sinh của bạn. <a href="/user/birthday" class="btn btn-info pull-right" style="padding: 2px 7px">Go!</a>
        </div>
    </div>
    @endif
    <br>
</div>
<div class="clearfix"></div>
<p class="text-muted" style="margin-bottom: 5px">Người dùng mới <a href="/user/all" class="m-l-10"> xem tất cả</a></p>
<div class="clearfix new-user">
    @foreach($takeTenNewUsers as $user)
    <a href="{{$user->facebook_url}}" target="_blank" data-toggle="tooltip" data-placement="top" title="{{$user->getShortName()}} ({{$user->getHoroscope()}})"><img data-original="{{$user->avatar}}" class="img-responsive lazy" width="50" height="50" alt="người dùng mới"></a>
    @endforeach
</div>
<div class="row">
    @foreach($horoscope as $key => $horo)
    <div class="col-md-6">
        <a href="/{{App\Helper\Bocap::getSlug($key)}}" class="title-item horo-title" style="background-color: {{$horo['color']}}">{{$horo['name']}}</a> <span class="m-l-5 text-muted">{{$horo['date']}}</span>
        <div class="category-box">
        @foreach($result[$key] as $i => $user)
        <a class="item-index"><img data-original="{{$user->avatar}}?height=75&width=75" class="img-responsive img-hover-shadow lazy" height="75" width="75">{{$user->getShortName()}}</a>

        @endforeach
        <a class="view-more" href="/{{App\Helper\Bocap::getSlug($key)}}">Xem thêm {{$horo['name']}}</a>
        </div>
    </div>
    @endforeach
</div>
<div class="">
    <p class="text-muted">Bạn có biết những người này không?</p>
    @foreach($otherUsers as $user)
        <a href="{{$user->facebook_url}}" target="_blank" data-toggle="tooltip" data-placement="top" title="{{$user->getShortName()}}" style="float: left;margin: 5px 10px 5px 0;"><img data-original="{{$user->avatar}}" class="img-responsive lazy" width="50" height="50"></a>
    @endforeach
    <div class="clearfix"></div>
    <br>
    <div class="fb-like" data-href="http://bocap.net/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
</div>
@if (Auth::guest())
<style type="text/css">
    #login-dialog .modal-body{
        background-image: url('/images/login-bg.jpg');
        background-size: cover;
        color: #fff;
    }
    #login-dialog .modal-body h3{
        color: #fff;
        font-weight: bolder;
    }
</style>
<div class="modal fade" id="login-dialog" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h3 style="color:red;"><span class="glyphicon glyphicon-lock"></span>Chào mừng bạn đến với website Bọ Cạp</h3>
            </div>
            <div class="modal-body">
                <label>Vui lòng đăng nhập để tiếp tục:</label>
                <h3>- Biết được bạn thuộc cung nào</h3>
                <h3>- Tìm kiếm và kết bạn với các cung hoàng đạo khác</h3>
                <h3>- Sử dụng vô số các tính năng thú vị dành riêng cho bạn</h3>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary btn-lg pull-left" href="/auth/facebook"><i class="fa fa-lg fa-facebook-square"></i> Đăng nhập qua Facebook</a>
            </div>
        </div>
    </div>
</div>
@endif
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@endsection

@section('extrascripts')
@if (Auth::guest())
<script type="text/javascript" src="/js/login-dialog.js"></script>
@endif
@endsection