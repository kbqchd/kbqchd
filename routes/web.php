<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/auth/facebook', 'Auth\FacebookAuthController@redirect');
Route::get('/auth/facebook/callback', 'Auth\FacebookAuthController@callback');

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/user', 'UserController@getIndex');
Route::get('/user/all', 'UserController@getAll');
Route::get('/user/account', 'UserController@getAccount');
Route::get('/user/create', 'UserController@getCreate');
Route::post('/user/create', 'UserController@postCreate');
Route::get('/user/birthday', 'UserController@getBirthday');
Route::post('/user/birthday', 'UserController@postBirthday');

Route::get('/slogan/minh-thich-thi-minh-lam-thoi', 'SloganController@getMinhThichThiMinhLamThoi');
Route::post('/slogan/minh-thich-thi-minh-lam-thoi', 'SloganController@postMinhThichThiMinhLamThoi');
Route::post('/slogan/delete', 'SloganController@postDeleteImage');

Route::get('/category/{id}', 'CategoryController@getView');
Route::get('/{slug}', 'CategoryController@getViewBySlug');

